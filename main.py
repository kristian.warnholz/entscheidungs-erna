"""
Simple bot which does good stuff on the 'Nos similis ludere' discord server.
"""

import discord
from discord.ext import commands
import os
from dotenv import load_dotenv
import random as rand

load_dotenv()

def is_str_int(string):
    """
    Tests if a string represents an int or not.

    Args:
    -----
    string (str): string to be tested

    Returns:
    --------
    (bool): True or False
    """
    try:
        int(string)
        return True
    except ValueError:
        return False

def catan_rand():
    """
    Returns random Catan game mode.

    Args:
    -----
    None.

    Returns:
    --------
    (str): random Catan game mode
    """
    modes = ['das Basisspiel',
             'Seefahrer: Zu neuen Ufern',
             'Seefahrer: Die vier Inseln',
             'Seefahrer: Durch die Wüste',
             'Seefahrer: Ozeanien']
    return rand.choice(modes)

def erna_rand():
    """
    Returns random Erna slogan.

    Args:
    -----
    None.

    Returns:
    --------
    (str): random slogan
    """
    erna_refs = ['Ohne Helm und ohne Gurt, einfach... Erna!',
                 'Erna ist wieder da!',
                 'Bonjour! Je m\'appelle Erna.',
                 'Wusstest du schon? Am ersten Sonntag im Oktober ist Ernadankfest!',
                 'Der Name ist Erna - Erna Richard Nordpol Anton.',
                 'FREUDE SCHÖNER GÖTTERFUNKEN! 🇪🇺']
    return rand.choice(erna_refs)

def int_to_roman(value):
    """
    Converts any int from (1, 3999) to a Roman numeral.

    Args:
    -----
    value (int): int from range (1, 3999)

    Returns:
    --------
    (str): converted value
    """
    definitions = {1000: 'M',
                   900: 'CM',
                   500: 'D',
                   400: 'CD',
                   100: 'C',
                   90: 'XC',
                   50: 'L',
                   40: 'XL',
                   10: 'X',
                   9: 'IX',
                   5: 'V',
                   4: 'IV',
                   1: 'I'}
    roman_num = ''
    for i in definitions:
        count = int(value / i)
        roman_num += definitions[i] * count
        value -= i * count
    return roman_num

def alea_rand(N):
    """
    Returns random dice value in Roman numerals in range (1, N).
    N must be an int from the range (1, 3999)

    Args:
    -----
    N (int): maximum value

    Returns:
    --------
    (str): random Roman numeral
    """
    x = int_to_roman(rand.randint(1, N))
    return(x)

def main():
    bot = commands.Bot(command_prefix='$', help_command = None)

    global baerenkatapult
    baerenkatapult = 0

    @bot.command(pass_context=True)
    async def help(ctx):
        await ctx.send('Ich kann noch nicht so viel, aber ein bisschen:\n'
                       '**$help**: Obviöslich erfährst du so mehr über meine '
                       'Funktionen.\n'
                       '**$alea [n]**: Würfelt mit einem Dn (1 ≤ n ≤ 3999). '
                       'Standardmäßig würfel ich mit einem D6 (n = 6).\n'
                       '**$catan**: Entscheidet zufällig einen CATAN-Modus.\n'
                       '**$erna**: Tja.')

    @bot.command(pass_context=True)
    async def catan(ctx):
        await ctx.send('Man spiele {}.'.format(catan_rand()))

    @bot.command(pass_context=True)
    async def erna(ctx):
        await ctx.send('{}'.format(erna_rand()))

    @bot.command(pass_context=True)
    async def alea(ctx, N=6):
        if is_str_int(N):
            if int(N) in range(1, 4000):
                await ctx.send('Alea iacta est: {}'.format(alea_rand(int(N))))
            else:
                await ctx.send('Entweder ist die Zahl zu groß oder zu klein. '
                               'Wie dem auch sei... Mein kleinster Würfel ist '
                               'ein D1 und mein größter ist ein D3999, '
                               'dazwischen hast du freie Auswahl.')
        else:
            await ctx.send('Ich bin ein integerer Bot, '
                           'aber diese Zahl ist nicht integer.')

    @bot.event
    async def on_ready():
        print('We have logged in as {0.user}'.format(bot))
        #await bot.change_presence(activity=discord.Game(name="in debugging mode."))
        await bot.change_presence(activity=discord.Game(name="in tested mode."))

    @bot.event
    async def on_message(message):
        global baerenkatapult
        if message.author == bot.user:
            pass
        elif any(key in message.content for key in ['AWESOME']):
            await message.channel.send('BÄRENKATAPULT!')
            baerenkatapult = 1
        elif any(key in message.content for key in ['Was?']) and baerenkatapult <= 2:
            await message.channel.send('BÄRENKATAPULT!')
            baerenkatapult += 1
        elif any(key in message.content for key in ['Warum?']) and baerenkatapult == 3:
            await message.channel.send('FICK DICH, DESHALB!')
            baerenkatapult = 0
        elif not message.content.startswith('$'):
            if 'Erna!' in message.content:
                if any(key in message.content for key in ['würfel', 'Würfel',
                                                          'alea', 'Alea']):
                    await message.channel.send('Habe ich was von Würfeln '
                                               'gehört? Leider habe ich gerade '
                                               'nur einen D6 zur Hand. Tja.')
                    await alea(message.channel)
                elif any(key in message.content for key in ['Catan', 'CATAN',
                                                            'Siedler',
                                                            'colonist',
                                                            'Colonist']):
                    await message.channel.send('Oh! Hört! Hört! Die feinen '
                                               'Leute wollen CATAN spielen. '
                                               'Wenn ich Sie mit meiner '
                                               'Randomisierungsexpertise '
                                               'inkommodieren darf:')
                    await catan(message.channel)
                elif any(key in message.content for key in ['Hilf', 'hilf',
                                                            'Helfe', 'helfe']):
                    await help(message.channel)
                else:
                    await message.channel.send('Erna ist anwesend.')
            elif any(key in message.content for key in ['Erna', 'erna']):
                await message.channel.send('Ich habe meinen Namen vernommen! '
                                           'Das ist doch schon mal was. :D '
                                           'Ich verstehe nicht immer, was man '
                                           'von mir möchte, aber ich gebe mein '
                                           'bestes. Schreibe \'$help\', um zu '
                                           'erfahren, was ich kann.')
            elif any(key in message.content for key in ['colonist.io']):
                await message.channel.send('Oh! Hört! Hört! Die feinen '
                                           'Leute wollen CATAN spielen. '
                                           'Wenn ich Sie mit meiner '
                                           'Randomisierungsexpertise '
                                           'inkommodieren darf:')
                await catan(message.channel)
            else:
                pass
        await bot.process_commands(message)

    bot.run(os.getenv('TOKEN'))

if __name__ == '__main__':
    main()
